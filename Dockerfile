
FROM gradle:4.8-jdk8-alpine as builder
WORKDIR /app
COPY build.gradle .
COPY gradle.properties .
COPY src ./src
USER root
RUN gradle build -x test


FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim
COPY --from=builder /app/build/libs/*-all.jar micronaut-jpa-gcloud.jar
CMD java  -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Dcom.sun.management.jmxremote -noverify ${JAVA_OPTS} -jar micronaut-jpa-gcloud.jar
