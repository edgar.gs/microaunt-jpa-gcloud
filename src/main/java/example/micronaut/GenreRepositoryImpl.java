package example.micronaut;

import example.micronaut.domain.Client;
import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.spring.tx.annotation.Transactional;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Singleton // <1>
public class GenreRepositoryImpl implements GenreRepository {

    @PersistenceContext
    private EntityManager entityManager;  // <2>
    private final ApplicationConfiguration applicationConfiguration;

    public GenreRepositoryImpl(@CurrentSession EntityManager entityManager,
                               ApplicationConfiguration applicationConfiguration) { // <2>
        this.entityManager = entityManager;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override
    @Transactional(readOnly = true) // <3>
    public Optional<Client> findById(@NotNull Long id) {
        return Optional.ofNullable(entityManager.find(Client.class, id));
    }

    @Override
    @Transactional // <4>
    public Client save(@NotNull String name,
                       @NotNull String lastname,
                       @NotNull int age,
                       @NotNull Date birth_date) {
        Client client = new Client(name, lastname, age, birth_date);
        entityManager.persist(client);
        return client;
    }

    @Override
    @Transactional
    public void deleteById(@NotNull Long id) {
        findById(id).ifPresent(client -> entityManager.remove(client));
    }

    private final static List<String> VALID_PROPERTY_NAMES = Arrays.asList("id", "name");

    @Transactional(readOnly = true)
    public List<Client> findAll(@NotNull SortingAndOrderArguments args) {
        String qlString = "SELECT g FROM Client as g";
        if (args.getOrder().isPresent() && args.getSort().isPresent() && VALID_PROPERTY_NAMES.contains(args.getSort().get())) {
                qlString += " ORDER BY g." + args.getSort().get() + " " + args.getOrder().get().toLowerCase();
        }
        TypedQuery<Client> query = entityManager.createQuery(qlString, Client.class);
        query.setMaxResults(args.getMax().orElseGet(applicationConfiguration::getMax));
        args.getOffset().ifPresent(query::setFirstResult);

        return query.getResultList();
    }

    @Override
    @Transactional
    public int update(@NotNull Long id, @NotNull String name,
                      @NotNull String lastname,
                      @NotNull int age,
                      @NotNull Date birth_date) {
        return entityManager.createQuery("UPDATE Client g SET name = :name, lastname = :lastname where id = :id")
                .setParameter("name", name)
                .setParameter("lastname", lastname)
                .setParameter("id", id)
                .executeUpdate();
    }
}
