package example.micronaut;

import example.micronaut.domain.Client;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.validation.Validated;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@Validated // <1>
@Controller("/clients") // <2>
public class GenreController {

    protected final GenreRepository genreRepository;

    public GenreController(GenreRepository genreRepository) { // <3>
        this.genreRepository = genreRepository;
    }

    @Get("/{id}") // <4>
    public Client show(Long id) {
        return genreRepository
                .findById(id)
                .orElse(null); // <5>
    }

    @Put("/") // <6>
    public HttpResponse update(@Body @Valid GenreUpdateCommand command) { // <7>
        int numberOfEntitiesUpdated = genreRepository.update(command.getId(), command.getName(),
                                                                                command.getLastname(),
                                                                                command.getAge(),
                                                                                command.getBirth_date());

        return HttpResponse
                .noContent()
                .header(HttpHeaders.LOCATION, location(command.getId()).getPath()); // <8>
    }

    @Get(value = "/list{?args*}") // <9>
    public List<Client> list(@Valid SortingAndOrderArguments args) {
        return genreRepository.findAll(args);
    }

    @Post("/") // <10>
    public HttpResponse<Client> save(@Body @Valid GenreSaveCommand cmd) {
        Client client = genreRepository.save(cmd.getName(), cmd.getLastname(), cmd.getAge(), cmd.getBirth_date());

        return HttpResponse
                .created(client)
                .headers(headers -> headers.location(location(client.getId())));
    }

    @Delete("/{id}") // <11>
    public HttpResponse delete(Long id) {
        genreRepository.deleteById(id);
        return HttpResponse.noContent();
    }

    protected URI location(Long id) {
        return URI.create("/clients/" + id);
    }

    protected URI location(Client client) {
        return location(client.getId());
    }
}
