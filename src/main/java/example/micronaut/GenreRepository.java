package example.micronaut;

import example.micronaut.domain.Client;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface GenreRepository {

    Optional<Client> findById(@NotNull Long id);

    Client save(@NotNull String name,
                @NotNull String lastname,
                @NotNull int age,
                @NotNull Date birth_date);

    void deleteById(@NotNull Long id);

    List<Client> findAll(@NotNull SortingAndOrderArguments args);

    int update(@NotNull Long id, @NotNull String name,
                                   @NotNull String lastname,
                                   @NotNull int age,
                                   @NotNull Date brith_date);
}
