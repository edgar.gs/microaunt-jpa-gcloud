package example.micronaut;

import static org.junit.Assert.assertEquals;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;

import example.micronaut.domain.Client;
import io.micronaut.context.ApplicationContext;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.runtime.server.EmbeddedServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

public class ClientControllerTest {

    private static EmbeddedServer server; // <1>
    private static HttpClient client; // <2>

    @BeforeClass
    public static void setupServer() {
        server = ApplicationContext
                .build()
                .run(EmbeddedServer.class); // <1>
        client = server.getApplicationContext().createBean(HttpClient.class, server.getURL()); // <2>
    }

    @AfterClass
    public static void stopServer() {
        if (server != null) {
            server.stop();
        }
        if (client != null) {
            client.stop();
        }
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void supplyAnInvalidOrderTriggersValidationFailure() {
        thrown.expect(HttpClientResponseException.class);
        thrown.expect(hasProperty("response", hasProperty("status", is(HttpStatus.BAD_REQUEST))));
        client.toBlocking().exchange(HttpRequest.GET("/genres/list?order=foo"));
    }

    @Test
    public void testFindNonExistingGenreReturns404() {
        thrown.expect(HttpClientResponseException.class);
        thrown.expect(hasProperty("response", hasProperty("status", is(HttpStatus.NOT_FOUND))));
        HttpResponse response = client.toBlocking().exchange(HttpRequest.GET("/genres/99"));
    }

    @Test
    public void testGenreCrudOperations() {

        List<Long> genreIds = new ArrayList<>();

        HttpRequest request = HttpRequest.POST("/clients", new GenreSaveCommand("DevOps")); // <3>
        HttpResponse response = GenreControllerTest.client.toBlocking().exchange(request);
        genreIds.add(entityId(response));

        assertEquals(HttpStatus.CREATED, response.getStatus());

        request = HttpRequest.POST("/clients", new GenreSaveCommand("Microservices")); // <3>
        response = GenreControllerTest.client.toBlocking().exchange(request);

        assertEquals(HttpStatus.CREATED, response.getStatus());

        Long id = entityId(response);
        genreIds.add(id);
        request = HttpRequest.GET("/clients/"+id);

        Client client = GenreControllerTest.client.toBlocking().retrieve(request, Client.class); // <4>

        assertEquals("Microservices", client.getName());

        request = HttpRequest.PUT("/clients", new GenreUpdateCommand(id, "Micro-services"));
        response = GenreControllerTest.client.toBlocking().exchange(request);  // <5>

        assertEquals(HttpStatus.NO_CONTENT, response.getStatus());

        request = HttpRequest.GET("/clients/" + id);
        client = GenreControllerTest.client.toBlocking().retrieve(request, Client.class);
        assertEquals("Micro-services", client.getName());

        request = HttpRequest.GET("/clients/list");
        List<Client> clients = GenreControllerTest.client.toBlocking().retrieve(request, Argument.of(List.class, Client.class));

        assertEquals(2, clients.size());

        request = HttpRequest.GET("/clients/list?max=1");
        clients = GenreControllerTest.client.toBlocking().retrieve(request, Argument.of(List.class, Client.class));

        assertEquals(1, clients.size());
        assertEquals("DevOps", clients.get(0).getName());

        request = HttpRequest.GET("/clients/list?max=1&order=desc&sort=name");
        clients = GenreControllerTest.client.toBlocking().retrieve(request, Argument.of(List.class, Client.class));

        assertEquals(1, clients.size());
        assertEquals("Micro-services", clients.get(0).getName());

        request = HttpRequest.GET("/clients/list?max=1&offset=10");
        clients = GenreControllerTest.client.toBlocking().retrieve(request, Argument.of(List.class, Client.class));

        assertEquals(0, clients.size());

        // cleanup:
        for (Long genreId : genreIds) {
            request = HttpRequest.DELETE("/clients/"+genreId);
            response = GenreControllerTest.client.toBlocking().exchange(request);
            assertEquals(HttpStatus.NO_CONTENT, response.getStatus());
        }
    }

    protected Long entityId(HttpResponse response) {
        String path = "/genres/";
        String value = response.header(HttpHeaders.LOCATION);
        if ( value == null) {
            return null;
        }
        int index = value.indexOf(path);
        if ( index != -1) {
            return Long.valueOf(value.substring(index + path.length()));
        }
        return null;
    }
}
